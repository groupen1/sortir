<?php


namespace App\Data;



use App\Entity\Campus;
use App\Entity\Etat;
use App\Entity\Utilisateur;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Validator\Constraints as Assert;

class SearchData
{

    /**
     * @var string
     */
    public $q = '';

    /**
     * @var Campus[]
     */
    public $campus = [];

    /**
     * @var Etat[]
     */
    public $etat = [];

    /**
     * @Assert\DateTime
     * @var string A "Y-m-d H:i:s" formatted value
     */
    public $dateEntreDebut;

    /**
     * @Assert\DateTime
     * @var string A "Y-m-d H:i:s" formatted value
     */
    public $dateEntreFin;

    /**
     * @var Boolean
     */
    public $jOrganise;

    /**
     * @var Boolean
     */
    public $suisInscrit;

    /**
     * @var Boolean
     */
    public $pasInscrit;

    /**
     * @var Boolean
     */
    public $sortiesPasse;





}