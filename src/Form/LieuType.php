<?php

namespace App\Form;

use App\Entity\Lieu;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LieuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', null, ['label'=>'Nom du lieu'])
            ->add('rue')
            ->add('latitude',null,[
                'label_attr' => ['style'=>'display:none'],
                'attr' => ['style'=>'display:none',
                    'value'=>'0']
            ])
            ->add('longitude',null,[
                'label_attr' => ['style'=>'display:none'],
                'attr' => ['style'=>'display:none',
                    'value'=>'0']
            ])
            ->add('ville', null,[
                'label_attr' => ['style'=>'display:none'],
                    'attr' => ['style'=>'display:none',
                        'value'=>'0']
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lieu::class,
        ]);
    }
}
