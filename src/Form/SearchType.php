<?php


namespace App\Form;


use App\Data\SearchData;
use App\Entity\Campus;

use App\Entity\Utilisateur;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('q', TextType::class,[
                'label' => "Par mot clé",
                'required' => false,
                'attr' =>[
                    'placeholder' => 'Rechercher'
                ]
            ])
            ->add('campus', EntityType::class,[
                'label'=>"campus",
                'required' => false,
                'class' => Campus::class,
                'expanded' => false,
                'choice_label' => 'nom'



            ])
            ->add('dateEntreDebut', DateType::class,[
                'label' => 'Entre',
                'required' => false
            ])
            ->add('dateEntreFin', DateType::class,[
                'label'=> 'et',
                'required'=> false
            ])
            //TODO vOIR POUR FILTRER SI J4ORGANISE OU PAS
            ->add('jOrganise', CheckboxType::class,[
                'label'=>'Sorties que j\'organise',
                'required'=> false
            ])
            ->add('suisInscrit', CheckboxType::class,[
                'label'=>'Sorties auxquelles je suis inscrit',
                'required'=> false
            ])
            ->add('pasInscrit', CheckboxType::class,[
                'label'=>'Sorties auxquelles je ne suis pas inscrit ',
                'required'=> false
            ])
            //TODO a voir pour rajouter les sorties passé. La flemme la
            /*->add('sortiesPasse', CheckboxType::class,[
                'label'=>'Sorties passées',
                'required'=> false
            ])*/


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
      $resolver->setDefaults([
          'data_class' => SearchData::class,
          'method' => 'GET',
          'csrf_protection' => false

      ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }

}