<?php

namespace App\Form;

use App\Entity\Sortie;
use App\Entity\Lieu;
use App\Entity\Ville;
use App\Entity\Utilisateur;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Date;

class SortieType extends AbstractType
{
        public function buildForm(FormBuilderInterface $builder, array $options)
        {

            //TODO voir pour mettre une date dans le placeholder
            //TODO A revoir pour mettre ville à la place de lieu. Ca ne marche pas je crois car il n'y a pas de relation direct entre sortie et ville
            $builder
                ->add('nom',null,[
                'label' => 'Nom de la sortie'])
                ->add('dateLimiteInscription')
                ->add('dateHeureDebut')
                ->add('duree')
                ->add('ville',EntityType::class,[
                    'placeholder' => 'Choisissez une ville',
                    'class' => Ville::class,
                    'label'=>'Ville',
                    'attr' =>[ 'data-source' => '../public/choixVille/$id']


                 ])
                ->add('lieu', EntityType::class,
                    [
                        'class' => Lieu::class,
                        'choice_label' => 'nom',
                        'empty_data' => null,
                        'placeholder' => 'Choisissez un lieu',
                        'label_attr' => ['id'=> 'lieuxLabel', 'style' => 'display:none'],
                        'attr' => ['style' => 'display:none']
                    ])


                ->add('nbInscriptionsMax',null,[
                    'attr' => ['min '=> '0']
                ])
                ->add('infosSortie', null, ["label" => "Description Sortie"])


            ;
        }

        public function configureOptions(OptionsResolver $resolver)
        {
            $resolver->setDefaults([
                'data_class' => Sortie::class,
            ]);
        }
}
