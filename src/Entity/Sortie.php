<?php

namespace App\Entity;
use App\Repository\SortieRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SortieRepository::class)
 */
class Sortie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Metter le nom de la sortie")
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @Assert\NotBlank(message="Metter la date du débit de la sortie")
     * @Assert\GreaterThan("today")
     * @ORM\Column(type="datetime")
     */
    private $dateHeureDebut;

    /**
     * @Assert\NotBlank(message="Metter la durée de sortie")
     * @ORM\Column(type="time")
     */
    private $duree;

    /**
     * @Assert\NotBlank(message="Metter la date limite d'inscription")
     * @Assert\GreaterThan("today")
     * @ORM\Column(type="datetime")
     */
    private $dateLimiteInscription;

    /**
     * @Assert\NotBlank(message="Metter le nombre max de personne pour la sortie")
     * @ORM\Column(type="integer")
     */
    private $nbInscriptionsMax;

    /**
     * @Assert\NotBlank(message="Metter les infos de la sortie sinon ça sert à rien")
     * @ORM\Column(type="string", length=255)
     */
    private $infosSortie;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Etat", inversedBy="sorties")
     * @ORM\JoinColumn(nullable=false)
     */
    private $etat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Utilisateur", inversedBy="sorties", cascade={"remove"}, cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $organisateur;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Utilisateur", cascade={"persist"})
     */
    private $utilisateurs;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Campus", inversedBy="sorties")
     * @ORM\JoinColumn(nullable=false)
     */
    private $campusOrganisateur;

    /**
     * @Assert\NotBlank(message="Metter le nom du lieu")
     * @ORM\ManyToOne(targetEntity="App\Entity\Lieu", inversedBy="sorties")
     */
    private $lieu;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateHeureDebut(): ?\DateTimeInterface
    {
        return $this->dateHeureDebut;
    }

    public function setDateHeureDebut(\DateTimeInterface $dateHeureDebut): self
    {
        $this->dateHeureDebut = $dateHeureDebut;

        return $this;
    }

    public function getDuree(): ?\DateTimeInterface
    {
        return $this->duree;
    }

    public function setDuree(\DateTimeInterface $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getDateLimiteInscription(): ?\DateTimeInterface
    {
        return $this->dateLimiteInscription;
    }

    public function setDateLimiteInscription(\DateTimeInterface $dateLimiteInscription): self
    {
        $this->dateLimiteInscription = $dateLimiteInscription;

        return $this;
    }

    public function getNbInscriptionsMax(): ?int
    {
        return $this->nbInscriptionsMax;
    }

    public function setNbInscriptionsMax(int $nbInscriptionsMax): self
    {
        $this->nbInscriptionsMax = $nbInscriptionsMax;

        return $this;
    }

    public function getInfosSortie(): ?string
    {
        return $this->infosSortie;
    }

    public function setInfosSortie(string $infosSortie): self
    {
        $this->infosSortie = $infosSortie;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(Etat $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function __construct()
    {
        $this->date = new \Datetime();
        $this->utilisateurs = new ArrayCollection();
    }

    public function addUtilisateur(Utilisateur $Utilisateur)
    {
        $this->utilisateurs[] = $Utilisateur;
        $Utilisateur->addSortie($this);
        return $this;
    }

    public function removeUtilisateur(Utilisateur $Utilisateur)
    {
        $this->utilisateurs->removeElement($Utilisateur);
    }


    /**
     * @return mixed
     */
    public function getOrganisateur()
    {
        return $this->organisateur;
    }

    /**
     * @param mixed $Organisateur
     */
    public function setOrganisateur(Utilisateur $Organisateur): void
    {
        $this->organisateur = $Organisateur;
    }

    /**
     * @return ArrayCollection
     */
    public function getUtilisateurs()
    {
        return $this->utilisateurs;
    }

    /**
     * @param mixed $Utilisateurs
     */
    public function setUtilisateurs($Utilisateurs): void
    {
        $this->utilisateurs = $Utilisateurs;
    }

    /**
     * @return mixed
     */
    public function getCampusOrganisateur()
    {
        return $this->campusOrganisateur;
    }

    /**
     * @param mixed $CampusOrganisateur
     */
    public function setCampusOrganisateur($CampusOrganisateur): void
    {
        $this->campusOrganisateur = $CampusOrganisateur;
    }

    /**
     * @return mixed
     */
    public function getLieu()
    {
        return $this->lieu;
    }
    /**
     * @return mixed
     */
    public function getVille()
    {
        if($this->lieu != NULL) {
            $this->lieu->getVille();
        }else{

        }
    }

    /**
     * @return mixed
     */
    public function setVille($ville)
    {
        if($this->lieu != NULL) {
            $this->lieu->setVille($ville);
        }

    }

    /**
     * @param mixed $Lieu
     */
    public function setLieu($Lieu): void
    {
        $this->lieu = $Lieu;
    }
    public function __toString()
    {
        return $this->nom;
    }


}
