<?php

namespace App\Controller;

use App\Entity\Campus;
use App\Entity\UploadCsv;
use App\Entity\Utilisateur;
use App\Form\ChangePasswordType;
use App\Form\ContactType;
use App\Form\PasswordType;
use App\Form\RegisterType;
use App\Form\UpdateUserType;
use App\Form\UploadCsvType;
use App\GetCurrentUser\GetCurrentUser;
use App\Repository\CampusRepository;
use App\Repository\UploadCsvRepository;
use App\Repository\UtilisateurRepository;
use Doctrine\DBAL\Driver\Mysqli\MysqliException;
use Doctrine\DBAL\Driver\SQLAnywhere\SQLAnywhereException;
use Doctrine\DBAL\Driver\SQLSrv\SQLSrvException;
use Doctrine\DBAL\SQLParserUtilsException;
use Doctrine\ORM\EntityManagerInterface;
use http\Client\Curl\User;
use League\Csv\Reader;
use phpDocumentor\Reflection\Types\This;
use PhpParser\Node\Scalar\String_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Faker;
use function Matrix\add;


class UserController extends AbstractController
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->render('main/home.html.twig');
    }

    /**
     * @Route("/login", name="login")
     */
    public function login()
    {
        return $this->render('user/login.html.twig');
    }
    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {

    }
    /**
     * @Route("/admin/register", name="register")
     */
    public function register(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder,
                             UtilisateurRepository $ur, ValidatorInterface $validator, TokenGeneratorInterface $token, \Swift_Mailer $mailer)
    {
        $utilisateur = New Utilisateur();
        $registerForm = $this->createForm(RegisterType::class, $utilisateur);
        $registerForm->handleRequest($request);
        $utilisateur->setRoles(["ROLE_USER"]);
        $utilisateur->setActif(1);
        if($registerForm->isSubmitted() && $registerForm->isValid()){
            $erreurs = $validator->validate($utilisateur);
            if(count($erreurs) > 0){
                $this->addFlash('erreurs', 'le username ou le mail est déja utilisé');
                return $this->redirectToRoute('register');

            }else {

                $hash = $encoder->encodePassword($utilisateur, $utilisateur->getPassword());
                $utilisateur->setPassword($hash);
                $utilisateur->setRoles(['ROLE_NOT_ACTIVATE']);

                $utilisateur->setActivationToken(md5(uniqid()));
                $em->persist($utilisateur);
                $em->flush();
                $token = $utilisateur->getActivationToken();
                $url = $this->generateUrl('activation_compte',['token'=>$token], UrlGeneratorInterface::ABSOLUTE_URL);
                $message = (new \Swift_Message('Activation compte'))
                    ->setFrom('send@hey.com')
                    ->setTo($utilisateur->getMail())
                    ->setBody(
                    // $this->renderView(
                    // 'contact/contact.html.twig',compact('contact')
                        'Lien pour réactiver le mot de passe de l\'adresse '.$url
                    //),'text/html'
                    )
                ;
                $mailer->send($message);
                $this->addFlash('success', 'L\'utilisateur a bien été inscrit');
                return $this->redirectToRoute('home');
            }
        }


        return $this->render('user/register.html.twig',[
            'registerForm'=>$registerForm->createView()
        ]);
    }

    /**
     * @Route("/updateUser", name="update_user")
     */
    public function updateUser(Request $request,EntityManagerInterface $em, UtilisateurRepository $ur, ValidatorInterface $validator)
    {

        $utilisateur = $this->getUser();
        $updateUserForm = $this->createForm(UpdateUserType::class, $utilisateur);

        $updateUserForm->handleRequest($request);
        if($updateUserForm->isSubmitted() && $updateUserForm->isValid()) {
            $erreurs = $validator->validate($utilisateur);
            if(count($erreurs) > 0){
                $this->addFlash('erreurs', 'le username ou le mail est déja utilisé');
                return $this->redirectToRoute('register');

            }else{
                /** @var  $uploadFile */
                $uploadFile = $updateUserForm['image']->getData();

                if (!empty($uploadFile)) {
                    $destination = $this->getParameter('kernel.project_dir') . '/public/uploads/ProfilImage';
                    $addresse = 'uploads/ProfilImage/';
                    $newFilename = $utilisateur->getUsername() . '-photoProfil.' . $uploadFile->guessExtension();
                    $uploadFile->move(
                        $destination,
                        $newFilename
                    );
                    $utilisateur->setURLImage($addresse.$newFilename);
                }
                $em->persist($utilisateur);
                $em->flush();
                $this->addFlash('success', 'Modification reussie');
            }
            return $this->redirectToRoute('update_user');
        }
        return $this->render('user/updateUser.html.twig',[
            'updateUserForm'=>$updateUserForm->createView()
        ]);
    }


    /**
     * @Route("/changePassword", name="change_password")
     */
    public function changePassword( Request $request, UserPasswordEncoderInterface $encoder,  UtilisateurRepository $ur,EntityManagerInterface $em)
    {
        $utilisateur = $this->getUser();
        $newPasswordForm = $this->createForm(ChangePasswordType::class, $utilisateur);



        $passBdd = $utilisateur->getPassword();

        $newPasswordForm->handleRequest($request);
        if($newPasswordForm->isSubmitted() && $newPasswordForm->isValid()) {

            $currentPassword = $_POST['_password'];
            $passEncode = $encoder->encodePassword($utilisateur, $currentPassword);

           if($passEncode===$passBdd){

                $newPassword = $encoder->encodePassword($utilisateur, $utilisateur->getPassword());
                $utilisateur->setPassword($newPassword);

                $em->persist($utilisateur);
                $em->flush();
                echo("<br> mdp change");
                return $this->redirectToRoute('update_user',[
                    'id'=>$utilisateur->getId()
                ]);
           }

            echo("<br>mdp non change");
        }



        return $this->render('user/changePassword.html.twig',[
        'newPasswordForm'=>$newPasswordForm->createView()
        ]);
    }

    /**
     * @Route("/afficherProfil/{id}", name="afficher_profil")
     */
    public function afficherProfil($id ,UtilisateurRepository $ur)
    {
        $user = $ur->findOneBy(array("id" => $id));
        return $this->render('user/afficherProfil.html.twig',[
            'user'=>$user
        ]);
    }

    /**
     * @Route("/admin/uploadCsvFile", name="upload_csv_file")
     */
    public function uploadCsvFile(Request $request, EntityManagerInterface $em, CampusRepository $cr,UserPasswordEncoderInterface $encoder, ValidatorInterface $validator, TokenGeneratorInterface $token, \Swift_Mailer $mailer)
    {
        $filesystem = new Filesystem();

        $uc = new UploadCsv();
        $CSVform = $this->createForm(UploadCsvType::class, $uc);
        $CSVform->handleRequest($request);


        if($CSVform->isSubmitted() && $CSVform->isValid()){
            /** @var $uploadFile */
            $uploadFile = $CSVform['file']->getData();

            if(!empty($uploadFile)){
                $destination = $this->getParameter('kernel.project_dir') . '/public/uploads/csv';
                $originalFileName = pathinfo($uploadFile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII;[^A-Za-z0-9_] remove; Lower()', $originalFileName);
                $newFilename = $safeFilename .'.'.date("d-m-y"). '.'. 'csv';


                $uploadFile->move(
                    $destination,
                    $newFilename
                );
                $uc->setFile($newFilename);
                $em->persist($uc);
                $em->flush();
                $reader = Reader::createFromPath('uploads/csv/'.$uc->getFile());
                $results = $reader->fetchAssoc();
                $allCampus = $cr->findAll();

                foreach ($results as $row) {
                    //dd($row);
                    $campus = new Campus();
                    //$campus->setNom($row['campus']);
                    $campus = $row['campus'];
                    $campusExist = false;
                    foreach($allCampus as $c){
                        //verification de la validité des infos
                        //dd($campus,$c);
                        if($c->getNom() ==  $campus and
                            $row['prenom'] != '' and
                            $row['nom'] != '' and
                            $row['mail'] != '' and
                            $row['username'] != ''){


                            $campus = $cr->find($c->getId());

                            $userUser = new Utilisateur();
                            $hash = $encoder->encodePassword($userUser,'yoyo');
                            $userUser->setPrenom($row['prenom']);
                            $userUser->setNom($row['nom']);
                            $userUser->setTelephone($row['telephone']);
                            $userUser->setPassword($hash);
                            $userUser->setMail($row['mail']);
                            $userUser->setUsername($row['username']);
                            $userUser->setActif(1);
                            $userUser->setRoles(["ROLE_NOT_ACTIVATE"]);
                            $userUser->setCampus($campus);


                            $errors = $validator->validate($userUser);
                            //dd($errors);
                            if (count($errors) > 0){
                                $this->addFlash('success', (string) $errors);
                            }else {
                                $em->persist($userUser);
                                $em->flush();
                                $userUser->setActivationToken(md5(uniqid()));
                                $token = $userUser->getActivationToken();
                                $url = $this->generateUrl('activation_compte',['token'=>$token], UrlGeneratorInterface::ABSOLUTE_URL);
                                $message = (new \Swift_Message('Activation compte'))
                                    ->setFrom('send@hey.com')
                                    ->setTo($userUser->getMail())
                                    ->setBody(
                                    // $this->renderView(
                                    // 'contact/contact.html.twig',compact('contact')
                                        'Lien pour réactiver le mot de passe de l\'adresse'.'<br>'.$url
                                    //),'text/html'
                                    )
                                ;

                            }



                        }
                    }
                }

                $em->remove($uc);
                $em->flush();
                $filesystem->remove('uploads/csv/'.$uc->getFile());
                $stringErrors = '';


                $this->addFlash('success', 'Le fichier CSV à été ajouté !');
                return $this->redirectToRoute('home',[
                    'errors'=>$stringErrors
                ]);

            }
            $stringErrors = '';
            $this->addFlash('success', 'Le fichier CSV à été ajouté !');
            return $this->redirectToRoute('home',[
                'errors'=>$stringErrors
            ]);
        }

        return $this->render('user/insertCSVFile.html.twig',[
            'csvForm'=> $CSVform->createView()
        ]);
    }

    /**
     * @Route("/oubliePassword", name="oublie_password")
     */
    public function oubliePassword(Request $request, \Swift_Mailer $mailer, UtilisateurRepository $ur, TokenGeneratorInterface $tokenGenerator,
                                   EntityManagerInterface $em)
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $contact = $form->getData();
            //dd($contact['mail']);
            $username =
            $user = $ur->findOneBy(['mail'=>$contact['mail']]);
            if(!$user){
                $this->addFlash('danger', 'Cette adresse n\'éxiste pas');
                return $this->redirectToRoute('oublie_password');
            }

            $token = $tokenGenerator->generateToken();

            try {
                $user->setResetToken($token);
                $em->persist($user);
                $em->flush();


            }catch(\Exception $e){
                $this->addFlash('warninge', 'Une erreur est survenue : '.$e->getMessage());
                return $this->redirectToRoute('oublie_password');

            }

            $url = $this->generateUrl('reset_password',['token'=>$token], UrlGeneratorInterface::ABSOLUTE_URL);


            $message = (new \Swift_Message('Mot de passe oublié'))
                ->setFrom('send@hey.com')
                ->setTo($user->getMail())
                ->setBody(
                   // $this->renderView(
                       // 'contact/contact.html.twig',compact('contact')
                        'Lien pour réactiver le mot de passe de l\'adresse'.'<br>'.$url
                    //),'text/html'
                )
            ;
            $mailer->send($message);
            $this->addFlash('succes','mail de réactualisation envoyé à votre adresse');
            return $this->redirectToRoute('login');
        }

        return $this->render('user/motDePasseOublie.html.twig',[
            'contactForm'=>$form->createView()
        ]);
    }

    /**
     * @Route("/resetPassword/{token}", name="reset_password")
     */
    public function resetPassword($token, Request $request, UserPasswordEncoderInterface $encoder, EntityManagerInterface $em, UtilisateurRepository $ur )
    {
        $user = $ur->findOneBy(['resetToken'=>$token]);
        if(!$user){
            $this->addFlash('danger', 'Lien déja utilisé');
            return $this->redirectToRoute('login');
        }
        if($request->isMethod('POST')){
            $user->setResetToken(null);
            $hash = $encoder->encodePassword($user, $request->request->get('password'));
            $user->setPassword($hash);
            $em->persist($user);
            $em->flush();
            $this->addFlash('message', 'Mot de passe modifié avec succès');

            return $this->redirectToRoute('login');
        }else{
            return $this->render('user/resetPassword.html.twig',['token'=>$token]);
        }


    }

    /**
     * @Route("admin/listeUtilisateurs", name="liste_utilisateurs")
     */
    public function listeUtilisateur(UtilisateurRepository $ur)
    {
        $users = $ur->findAll();
        return $this->render('/user/listeUtilisateurs.html.twig', [
                'utilisateurs'=>$users
        ]);
    }

    /**
     * @Route("admin/desactiverUtilisateur/{id}", name="desactiver_utilisateur")
     */
    public function desactiverUtilisateur($id,UtilisateurRepository $ur, EntityManagerInterface $em)
    {
        $user = $ur->find($id);
        $user->setActif(false);
        $em->persist($user);
        $em->flush();
        $users = $ur->findAll();

        $this->addFlash('success', 'l\'utilisateur à bien été désactiver');
        return $this->redirectToRoute('liste_utilisateurs',[
            'utilisateurs'=>$users
        ]);
    }

    /**
     * @Route("admin/supprimerUtilisateur/{id}", name="supprimer_utilisateur")
     */
    public function supprimerUtilisateur($id,UtilisateurRepository $ur, EntityManagerInterface $em)
    {
        $user = $ur->find($id);
        $em->remove($user);
        $em->flush();
        $users = $ur->findAll();

        $this->addFlash('success', 'l\'utilisateur à bien été supprimer');
        return $this->redirectToRoute('liste_utilisateurs',[
            'utilisateurs'=>$users
        ]);
    }

    /**
     * @Route("/activationCompte/{token}", name="activation_compte")
     */
    public function activerCompteUtilisateur($token, UtilisateurRepository $ur, EntityManagerInterface $em)
    {
        $user = $ur->findOneBy(['activationToken'=>$token]);
        if(!$user){
            $this->addFlash('erreur','l\'utilisateur n\'existe pas');
        }
        $user->setRoles(["ROLE_USER"]);
        $user->setActivationToken(null);
        $em->persist($user);
        $em->flush();

        $this->addFlash('success', 'l\'activation du compte à bien fonctionner');

        return $this->render('main/home.html.twig');

    }

    /**
     * @Route("/bannir/{id}", name="bannir")
     */
    public function bannir($id,UtilisateurRepository $ur, EntityManagerInterface $em)
    {
        $user = $ur->find($id);
        $user->setRoles(["ROLE_NOT_ACTIVATE"]);
        $em->persist($user);
        $em->flush();
        $users = $ur->findAll();

        $this->addFlash('success', 'l\'utilisateur à bien été désactiver');
        return $this->redirectToRoute('liste_utilisateurs',[
            'utilisateurs'=>$users
        ]);
    }

    /**
     * @Route("/debannir/{id}", name="debannir")
     */
    public function deBannir($id,UtilisateurRepository $ur, EntityManagerInterface $em)
    {
        $user = $ur->find($id);
        $user->setRoles(["ROLE_USER"]);
        $em->persist($user);
        $em->flush();
        $users = $ur->findAll();

        $this->addFlash('success', 'l\'utilisateur à bien été désactiver');
        return $this->redirectToRoute('liste_utilisateurs',[
            'utilisateurs'=>$users
        ]);
    }

}
