<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Lieu;
use App\Entity\Ville;
use App\Entity\Sortie;
use App\Entity\Security;
use App\Form\LieuType;
use App\Form\SearchType;
use App\Form\SortieType;
use App\Form\VilleType;
use App\GetCurrentUser\GetCurrentUser;
use App\Repository\EtatRepository;
use App\Repository\SortieRepository;
use App\Repository\UtilisateurRepository;
use App\Repository\LieuRepository;
use App\Repository\VilleRepository;
use phpDocumentor\Reflection\Types\Boolean;
use phpDocumentor\Reflection\Types\This;
use Proxies\__CG__\App\Entity\Utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use \Doctrine\Common\Collections\Criteria;
use function Doctrine\Common\Util\Debug;

/**
 * Class SortieController
 */
class SortieController extends AbstractController
{
    /**
     * @Route("/sorties", name="liste_sorties")
     */
    public function listeFiltrer(SortieRepository $sr,EtatRepository $er, Request $request)
    {
        $data = new SearchData();
        $form = $this->createForm(SearchType::class, $data);
        $form->handleRequest($request);



        //dd($sorties);
        if ($form->isSubmitted() && $form->isValid()) {
            //dd($data);
            $sorties = $sr->findSearch($data, $this->getUser());

            //dd($nombre);
            return $this->render('sortie/listesorties.html.twig', [
                'formFiltre' => $form->createView(),
                "sorties" => $sorties

            ]);
        }

        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->neq('etat', $er->findOneBy(array('libelle' => 'Ar'))));
        return $this->render('sortie/listesorties.html.twig', [
            'formFiltre' => $form->createView(),
            'sorties' => $sr->matching($criteria)

        ]);

    }

    /*/**
     * @Route("/sorties", name="liste_sorties")
     */
    /*public function listeFiltrer(SortieRepository $sr,Request $request)
    {
        $data = new SearchData();
        $sorties = $sr->findAll();
        $form = $this->createForm(SearchType::class, $data);
        $form->handleRequest($request);

        return $this->render('sortie/listesorties.html.twig', [
            "sorties" => $sorties,
            'formFiltre' => $form->createView()
        ]);
    }*/

    /**
     * @Route("/nouvelleSortie", name="nouvelle_sortie")
     */
    public function nouvelleSortie( Request $request, EntityManagerInterface $em, UtilisateurRepository $ur, SortieRepository $sortieRepositoryr, EtatRepository $er)
    {
        //TODO Prob pour le nouveau lieu car il faut creer la ville avant
        $sortie = new Sortie();
        $lieu = new Lieu();
        $ville = new Ville();
        $sortie->setDateHeureDebut(new \DateTime('now'));
        $sortie->setDateLimiteInscription(new \DateTime('now'));
        $nouvelleVille=0;
        $nouveauLieu=0;
        $idVille=0;
        $utilisateur = $this->getUser();
        $newSortieForm = $this->createForm(SortieType::class, $sortie);
        $newLieuForm = $this->createForm(LieuType::class, $lieu);
        $newVilleForm = $this->createForm(VilleType::class, $ville);

        $newLieuForm->handleRequest($request);
        $newSortieForm->handleRequest($request);
        $newVilleForm->handleRequest($request);

        if ($newSortieForm->isSubmitted() && $newSortieForm->isValid()){
            if($sortie->getDateHeureDebut() < $sortie->getDateLimiteInscription()){
                $this->addFlash('error','la date de la cloture ne peut pas être ultérieur à la date du début');
                return $this->redirectToRoute('nouvelle_sortie');
            }
        }

        if($newLieuForm->isSubmitted() && $newLieuForm->isValid()){
            $nouveauLieu = 1;
            $city = $lieu->getVille();
            $idVille=$city->getId();
            $em->persist($lieu);
            $em->flush();
            $this->addFlash('success', 'Enregistrement réussi');
            return $this->render('sortie/newSortie.html.twig', [
                'newSortieForm' => $newSortieForm->createView(),
                'newLieuForm' => $newLieuForm->createView(),
                'newVilleForm' => $newVilleForm->createView(),
                'nouvelleVille' => $nouvelleVille,
                'nouveauLieu' => $nouveauLieu,
                'idVille' => $idVille
                ]);
        }
        if($newVilleForm->isSubmitted() && $newVilleForm->isValid()){

            $em->persist($ville);
            $em->flush();
            $this->addFlash('success', 'Enregistrement réussi');
            $nouvelleVille=1;
            //return $this->redirectToRoute('nouvelle_sortie');
             return $this->render('sortie/newSortie.html.twig', [
                'newSortieForm' => $newSortieForm->createView(),
                'newLieuForm' => $newLieuForm->createView(),
                'newVilleForm' => $newVilleForm->createView(),
                'nouvelleVille' => $nouvelleVille,
                'nouveauLieu' => $nouveauLieu,
                 'idVille' => $idVille

            ]);
        }

        if ($newSortieForm->isSubmitted() && $newSortieForm->isValid()) {

            $sortie->setEtat($er->findOneBy(array('libelle' => 'Ou')));
            $sortie->setOrganisateur($utilisateur);
            $sortie->setCampusOrganisateur($utilisateur->getCampus());

            $em->persist($sortie);
            $em->flush();
            $this->addFlash('success', 'Enregistrement réussi');
            return $this->redirectToRoute('liste_sorties');

        }


        return $this->render('sortie/newSortie.html.twig', [
            'newSortieForm' => $newSortieForm->createView(),
            'newLieuForm' => $newLieuForm->createView(),
            'newVilleForm' => $newVilleForm->createView(),
            'nouvelleVille' => $nouvelleVille,
            'nouveauLieu' => $nouveauLieu,
            'idVille' => $idVille
        ]);
    }

    /**
     * @Route("/detailsSortie/{id}", name="details_sortie")
     */
    public function detailsSortie($id, SortieRepository $sr, UtilisateurRepository $ur, GetCurrentUser $getUser)
    {

        $boss = false;
        $sortie = $sr->find($id);
        $nombreParticipants = count($ur->getUsersParSortie($sortie));
        $participants = $ur->getUsersParSortie($sortie);
        $etat = $sortie->getEtat();
        $organisateur = $sortie->getOrganisateur();
        $user = $getUser->getCurrentUser();
        $cancel = false;


        foreach($ur->getUsersParSortie($sortie) as $u){
           if($u->getId() == $user->getId()){
               $result = true;
               $participants = $ur->getUsersParSortie($sortie);
           }else{
               $result = false;
               $tabNothing = [];
               $participants = $tabNothing;
           }
        }
        if(!isset($result)){
            $result = 1;
        }
        if($organisateur->getId() == $this->getUser()->getId()){
            $boss = true;
        }


        return $this->render('sortie/detailsSortie.html.twig', [
            'sortie' => $sortie,
            'nombreParticipants' => $nombreParticipants,
            'result'=> $result,
            'participants'=> $participants,
            'etat'=> $etat,
            'boss'=> $boss,
            'cancel'=>false
        ]);

    }

    /**
     * @Route("/inscriptionSortie/{id}", name="inscription_sortie")
     */
    public function inscriptionSortie($id, SortieRepository $sr, GetCurrentUser $getUser, EntityManagerInterface $em, UtilisateurRepository $ur)
    {

        $boss = false;
        $sortie = $sr->find($id);
        $user = $this->getUser();
        $etat = $sortie->getEtat();
        $cancel = false;
        $sortie->addUtilisateur($user);
        $em->persist($sortie);
        $em->flush();
        $sortie = $sr->find($id);
        foreach($ur->getUsersParSortie($sortie) as $u){
            if($u->getId() == $user->getId()){
                $result = true;
                $participants = $ur->getUsersParSortie($sortie);
            }else{
                $result = false;

            }
        }
        $nombreParticipants = count($ur->getUsersParSortie($sortie));

        $this->addFlash('success', 'Inscription reussi');
        return $this->redirectToRoute('details_sortie',[
            'id'=>$sortie->getId(),
            'sortie'=>$sortie,
            'nombreParticipants' => $nombreParticipants,
            'result'=> $result,
            'participants'=> $participants,
            'etat'=> $etat,
            'boss'=> $boss,
            'cancel'=>false
        ]);


    }

    /**
     * @Route("/desistementSorti/{id}", name="desistement_sortie")
     */
    public function desistementSortie($id, SortieRepository $sr, GetCurrentUser $getUser, EntityManagerInterface $em, UtilisateurRepository $ur)
    {

        $boss = false;
        $sortie = $sr->find($id);
        $user = $this->getUser();
        $etat = $sortie->getEtat();
        $cancel = false;
        $sortie->removeUtilisateur($user);
        $em->persist($sortie);
        $em->flush();
        $sortie = $sr->find($id);
        foreach($ur->getUsersParSortie($sortie) as $u){
            if($u->getId() == $user->getId()){
                $result = true;
                $participants = $ur->getUsersParSortie($sortie);

            }else{
                $result = false;
                $tabNothing = [];
                $participants = $tabNothing;


            }
        }

        //$participants = $ur->getUsersParSortie($sortie);
        $nombreParticipants = count($ur->getUsersParSortie($sortie));
        $this->addFlash('success', 'Désinscription reussi');
        return $this->redirectToRoute('details_sortie',[
            'id'=>$sortie->getId(),
            'sortie'=>$sortie,
            'nombreParticipants' => $nombreParticipants,
            'result'=> $result,
            'participants'=>$participants,
            'etat'=>$etat,
            'boss'=>$boss,
            'cancel'=>false
        ]);


    }

    /**
     * @Route("/annulerSortie/{id}", name="annuler_sortie")
     */
    public function annulerSortie($id, SortieRepository $sr, EntityManagerInterface $em, UtilisateurRepository $ur, EtatRepository $er)
    {
        $boss = false;
        $sortie = $sr->find($id);
        $user = $this->getUser();
        $organistaeur = $sortie->getOrganisateur();
        if($organistaeur->getId() == $user->getId()){
                $result = true;
                $participants = $ur->getUsersParSortie($sortie);
                $cancel = true;
                $etat = $er->findOneBy(['libelle'=>'An']);
                $result = 1;
                $sortie->setEtat($etat);
                $em->persist($sortie);
                $em->flush();
                $this->addFlash('success', 'Annulation sortie reussi');

        }else{
                $result = false;
                $tabNothing = [];
                $participants = $tabNothing;
                $etat = 0;
                $cancel = false;
                $this->addFlash('success', 'Annulation sortie reussi');


            }

        //$participants = $ur->getUsersParSortie($sortie);
        $nombreParticipants = count($ur->getUsersParSortie($sortie));
        return $this->render('sortie/detailsSortie.html.twig',[
            'sortie'=>$sortie,
            'nombreParticipants' => $nombreParticipants,
            'result'=> $result,
            'participants'=>0,
            'etat'=>$etat,
            'boss'=>$boss,
            'cancel'=>$cancel
        ]);
    }

    /**
     * @Route("/annulerSortieAdmin/{id}", name="annuler_sortie_admin")
     */
    public function annulerSortieAdmin($id, SortieRepository $sr, EntityManagerInterface $em, EtatRepository $er)
    {
        $sortie = $sr->find($id);
        $etat = $er->findOneBy(['libelle'=>'An']);
        $sortie->setEtat($etat);
        $em->persist($sortie);
        $em->flush();
        $sorties = $sr->findAll();

        return $this->redirectToRoute('liste_sorties',[
                'sorties'=>$sorties
            ]);
    }

    /**
     *@Route("/choixVille/{id}", name="choix_ville")
     */
    public function choixVille($id, LieuRepository $lr, VilleRepository $vr )
    {

        $data= array($lr->findBy(array("ville" => $vr->findBy(array("id" => $id)))),$vr->findBy(array("id" => $id)));

        return $this->json($data);
    }

        /*$tab=$ur->getUsersParSortie($sortie);
    echo("debut");
    foreach($ur->getUsersParSortie() as $u){
    echo($user->getNom());
    }
    echo(count($tab));*/


}
