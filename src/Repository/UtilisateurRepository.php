<?php

namespace App\Repository;

use App\Entity\Utilisateur;
use App\Entity\Sortie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * @method Utilisateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Utilisateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Utilisateur[]    findAll()
 * @method Utilisateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UtilisateurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Utilisateur::class);
    }

/*SELECT u.nom, us.sortie_id
FROM utilisateur as u
INNER JOIN sortie_utilisateur as us
ON u.id = us.utilisateur_id
INNER JOIN sortie as s
ON s.id = us.sortie_id
WHERE us.sortie_id = 26*/
public function getUsersParSortie(Sortie $sortie)
{
    $table = $this->getClassMetadata()->table["name"];

    $sql = "SELECT u.* "
             ." FROM ".$table." AS u "
             ." INNER JOIN sortie_utilisateur AS us "
             ." ON u.id= us.utilisateur_id "
             ." INNER JOIN sortie as s "
             ." ON s.id = us.sortie_id "
             ." WHERE us.sortie_id = ".$sortie->getId();

    $rsm = new ResultSetMappingBuilder($this->getEntityManager());
    $rsm->addEntityResult(Utilisateur::class, "u");

    // On mappe le nom de chaque colonne en base de données sur les attributs de nos entités
    foreach ($this->getClassMetadata()->fieldMappings as $obj) {
        $rsm->addFieldResult("u", $obj["columnName"], $obj["fieldName"]);
    }
    $stmt = $this->getEntityManager()->createNativeQuery($sql, $rsm);
    $stmt->execute();
    return $stmt->getResult();


}

    // /**
    //  * @return Utilisateur[] Returns an array of Utilisateur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Utilisateur
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
