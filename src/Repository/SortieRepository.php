<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Sortie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sortie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sortie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sortie[]    findAll()
 * @method Sortie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SortieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sortie::class);
    }

    /**
     * @param SearchData $search
     * @return Sortie[]
     */
    public function findSearch(SearchData $search, $currentUser): array
    {
        $query = $this
        ->createQueryBuilder('s')
        ->select('c','s','u')
        ->join('s.campusOrganisateur','c')
        ->join('c.utilisateurs','u');

        //TODO a revoir pour si j'appuie sur deux bouttons en même temps. La j'ai la flemme... 14/08/2020
        // chercher par mot clé
        if(!empty($search->q)){
            $query = $query
                ->andWhere('s.nom LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }
        // chercher par campus
        if(!empty($search->campus)){
            $query = $query
                ->andWhere('c.id IN (:campus)')
                ->setParameter('campus', $search->campus);
        }
        //chercher par date
        if(!empty($search->dateEntreDebut) && !empty($search->dateEntreFin)){
            $query = $query
                ->andWhere('s.dateHeureDebut BETWEEN :from AND :to')
                ->setParameter('from', $search->dateEntreDebut)
                ->setParameter('to', $search->dateEntreFin );
        }

        $org=$search->jOrganise;
        $inscrit=$search->suisInscrit;
        $nonInscrit=$search->pasInscrit;




        if($org && $inscrit && $nonInscrit){
            //ne rien faire
        }else if($org && $inscrit){
            $query = $query
                ->andWhere(':utilisateur MEMBER OF s.utilisateurs OR s.organisateur IN (:utilisateur)')
                ->setParameter(':utilisateur', $currentUser)
                ->setParameter('utilisateur', $currentUser);
        }else if($org && $nonInscrit){
            $query = $query
                ->andWhere('s.organisateur IN (:utilisateur) OR :utilisateur NOT MEMBER OF s.utilisateurs')
                ->setParameter(':utilisateur', $currentUser)
                ->setParameter('utilisateur', $currentUser);


        }else if($inscrit && $nonInscrit){

            //ne rien faire

        } else if($org){
            $query = $query
                ->andWhere('s.organisateur IN (:utilisateur)')
                ->setParameter('utilisateur', $currentUser);

        }else if($inscrit){
            $query = $query
                ->andWhere(':utilisateur MEMBER OF s.utilisateurs OR s.organisateur IN (:utilisateur)')
                ->setParameter(':utilisateur', $currentUser)
                ->setParameter('utilisateur', $currentUser);

        }else if($nonInscrit){
            $query = $query
                ->andWhere(':utilisateur NOT MEMBER OF s.utilisateurs AND s.organisateur NOT IN (:utilisateur)')
                ->setParameter(':utilisateur', $currentUser)
                ->setParameter('utilisateur', $currentUser);
    }



        //chercher si la sortie est passé
        /*if(!empty($search->dateEntreDebut) && !empty($search->dateEntreFin)){
            $query = $query
                ->andWhere('s.dateHeureDebut BETWEEN :from AND :to')
                ->setParameter('from', new \DateTime('1970-09-25') )
                ->setParameter('to', new \DateTime('now'));
        }*/


        return $query->getQuery()->getResult();
    }

}
