<?php

namespace App\DataFixtures;


use App\Entity\Campus;
use App\Entity\Lieu;
use App\Entity\Utilisateur;
use App\Entity\Ville;
use App\Entity\Etat;
use App\Entity\Sortie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use phpDocumentor\Reflection\Types\Array_;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Faker;
use function MongoDB\BSON\toJSON;


class AppFixtures extends Fixture
{

    function randomUser(ObjectManager $manager, $faker,$campus)
    {


            $user = new Utilisateur();
            $user->setNom($faker->name);
            $user->setPrenom($faker->firstName);
            $user->setTelephone($faker->phoneNumber);
            $user->setPassword($this->encoder->encodePassword($user, 'yoyo'));
            $user->setMail($faker->email);
            $user->setUsername($faker->userName);
            $user->setActif(1);
            $user->setRoles(["ROLE_USER"]);
            $user->setCampus($campus);
            $user->setURLImage('uploads/ProfilImage/default.png');

            $manager->persist($user);
            $manager->flush();
            return $user;

    }
    function randomPlace(ObjectManager $manager,$faker,$ville)
    {

            $lieu = new Lieu();
            $lieu->setNom($faker->country);
            $lieu->setRue($faker->address);
            $lieu->setVille($ville);

            $manager->persist($lieu);
            $manager->flush();

        return $lieu;
    }

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {

        $faker = Faker\Factory::create('fr_FR');

        $etat1 = new Etat();
        $etat2 = new Etat();
        $etat3 = new Etat();
        $etat4 = new Etat();
        $etat5 = new Etat();
        $etat6 = new Etat();

        $etat1->setLibelle('Ou');
        $etat2->setLibelle('Cl');
        $etat3->setLibelle('Ec');
        $etat4->setLibelle('Pa');
        $etat5->setLibelle('An');
        $etat6->setLibelle('Ar');

        $manager->persist($etat1);
        $manager->persist($etat2);
        $manager->persist($etat3);
        $manager->persist($etat4);
        $manager->persist($etat5);
        $manager->persist($etat6);
        $manager->flush();



       for($i=0;$i < 8 ; $i++){
                $ville = new Ville();
                $ville->setNom($faker->city);
                $ville->setCodePostal($faker->postcode);
                $manager->persist($ville);
                $manager->flush();

                $campusListe = ['Niort','Quimper','Angers','Rennes','Le Mans','Laval','La roche-sur-yon','Nantes'];
               $campus = new Campus();
               $campus->setNom($campusListe[$i]);
               $manager->persist($campus);
               $manager->flush();

            for($h = 0; $h<2; $h++) {
                $sortie = new Sortie();

                $sortie->setNom($faker->jobTitle);
                $sortie->setEtat($etat1);
                $sortie->setLieu($this->randomPlace($manager, $faker, $ville));
                $sortie->setDateHeureDebut($faker->dateTimeBetween('now','+1 years'));
                $sortie->setDateLimiteInscription($faker->dateTimeBetween('now',$sortie->getDateHeureDebut()));
                $sortie->setDuree($faker->dateTimeBetween('now','+1 years'));
                $sortie->setInfosSortie($faker->text);
                $sortie->setNbInscriptionsMax($faker->numberBetween(2, 20));

                echo($sortie->getInfosSortie());



                for ($k = 0; $k < 5; $k++) {

                    if ($k == 0) {
                        $organisateur = $this->randomUser($manager, $faker, $campus);
                        $sortie->setOrganisateur($organisateur);
                        $sortie->setCampusOrganisateur($organisateur->getCampus());
                    } else {

                        $sortie->addUtilisateur($this->randomUser($manager, $faker, $campus));
                    }

                }
                $manager->persist($sortie);
                $manager->flush();
            }
        }

        $userAdmin = new Utilisateur();
        $userAdmin->setNom('Lebowski');
        $userAdmin->setPrenom('Jeffrey');
        $userAdmin->setTelephone($faker->phoneNumber);
        $userAdmin->setPassword($this->encoder->encodePassword($userAdmin, 'yoyo'));
        $userAdmin->setMail('blaschka.jellyfish@gmail.com');
        $userAdmin->setUsername('The dude');
        $userAdmin->setActif(1);
        $userAdmin->setRoles(["ROLE_ADMIN"]);
        $userAdmin->setCampus($campus);

        $userUser = new Utilisateur();
        $userUser->setNom('Morrisson');
        $userUser->setPrenom('Jim');
        $userUser->setTelephone($faker->phoneNumber);
        $userUser->setPassword($this->encoder->encodePassword($userAdmin, 'yoyo'));
        $userUser->setMail($faker->email);
        $userUser->setUsername('jim');
        $userUser->setActif(1);
        $userUser->setRoles(["ROLE_USER"]);
        $userUser->setCampus($campus);

        $manager->persist($userAdmin);
        $manager->persist($userUser);
        $manager->flush();
    }
}
