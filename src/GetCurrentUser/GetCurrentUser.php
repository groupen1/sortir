<?php


namespace App\GetCurrentUser;
use App\Entity\Utilisateur;
use App\Repository\UtilisateurRepository;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;

class GetCurrentUser
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var
     */
    private $currentUser;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @return mixed
     */
    public function getCurrentUser()
    {
        $this->currentUser = $this->security->getUser();

        return $this->currentUser;
    }


}