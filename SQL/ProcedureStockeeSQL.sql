BEGIN
UPDATE Sortie SET
            etat_id = (SELECT etat.id
                        FROM etat
                        WHERE etat.libelle = 'Cl')
                WHERE (Sortie.id IN
                                (SELECT Sortie.id
                                    FROM Sortie
                                    INNER JOIN etat
                                    WHERE etat.libelle = 'Ou' AND NOW()>=Sortie.date_limite_inscription))
                 OR
               Sortie.id IN (
                        SELECT sortie.id
                            FROM
                            (SELECT COUNT(DISTINCT (u.id) )AS nbInscrits, sortie.id AS ids
                                FROM utilisateur AS u
                                INNER JOIN sortie_utilisateur AS us
                                ON u.id= us.utilisateur_id
                                INNER JOIN sortie
                                ON sortie.id = us.sortie_id
                                GROUP BY sortie.id)                                                 AS tab

                                INNER JOIN sortie
                                ON tab.ids = sortie.id

                                WHERE tab.nbInscrits >= sortie.nb_inscriptions_max)
                                 AND sortie.id IN (    SELECT s.id
                                                        FROM sortie as s
                                                        INNER JOIN etat
                                                        ON s.etat_id = etat.id
                                                        WHERE etat.libelle = 'Ou');


      UPDATE Sortie SET
		etat_id = (SELECT etat.id
					FROM etat
					WHERE etat.libelle = 'Ec')
		WHERE  Sortie.id IN (SELECT Sortie.id
                    FROM Sortie
                    INNER JOIN etat
                     WHERE (etat.libelle = 'Ou' OR etat.libelle = 'Cl')
                     AND NOW()>=Sortie.date_heure_debut AND NOW()<=ADDTIME(Sortie.date_heure_debut,Sortie.duree));

	 UPDATE Sortie SET
		etat_id = (SELECT etat.id
					FROM etat
					WHERE etat.libelle = 'Pa')
		WHERE  Sortie.id IN (SELECT Sortie.id
                    FROM Sortie
                    INNER JOIN etat
                    WHERE etat.libelle != 'Pa' AND  NOW()>=ADDTIME(Sortie.date_heure_debut,Sortie.duree));

     UPDATE Sortie SET
		etat_id = (SELECT etat.id
					FROM etat
					WHERE etat.libelle = 'Ar')
		WHERE  Sortie.id IN (SELECT Sortie.id
                    FROM Sortie
                    INNER JOIN etat
                    WHERE etat.libelle = 'Pa' AND  NOW()>=ADDTIME(Sortie.date_heure_debut,"30 0:0:0.0"));
  END